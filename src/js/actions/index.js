import { ADD_TODO, TOGGLE_TODO, FILTER_TODO, VisibilityFilters } from '../constants/action-types';

export function addTodo(text){
    return { type:ADD_TODO, payload:text }
}


export function filterTodo(filter){
    return { type:FILTER_TODO, filter:filter }
}

export function toggleTodo(id){
    return { type:TOGGLE_TODO, index:id }
}