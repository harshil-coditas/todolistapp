export const ADD_TODO="ADD_TODO";
export const TOGGLE_TODO="TOGGLE_TODO";
export const FILTER_TODO="FILTER_TODO";
export const VisibilityFilters={
    SHOW_ALL:"SHOW_ALL",
    SHOW_COMPLETED:"SHOW_COMPLETED",
    SHOW_ACTIVE:"SHOW_ACTIVE"
} 