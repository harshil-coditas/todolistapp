import React from "react";
import {connect} from "react-redux";
import {addTodo} from '../actions/index'
import uuidv1 from 'uuid';

const mapDispatchToProps = dispatch =>{
    return{
        addTodo: text => dispatch(addTodo(text))
    }
}

class TodoAdd extends React.Component{
    constructor(){
        super();
        this.state={
            value:""
        }
        this.handleChange=this.handleChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);

    }
    
    handleSubmit(event){
        event.preventDefault();
        const text=this.state.value;
        const newTodo={
            id: uuidv1(),
            todo: text,
            completed:false,
        }
        this.props.addTodo(newTodo);
        this.setState({value:""});
    }

    handleChange(event){
        this.setState({
            value:event.target.value,
        });
    }

    render(){
        const value=this.state.value;
        return(
            <div className="form">
                <form onSubmit={this.handleSubmit}>
                    <input type="text" onChange={this.handleChange} value={value} />
                    <button type="submit">Add TODO</button>
                </form>
            </div>
        )
    }
}


const AddTodo= connect(null,mapDispatchToProps)(TodoAdd);

export default AddTodo;

