import AddTodo from './AddTodo';
import FilterTodo from './FilterTodo';
import ListTodo from './ListTodo';
import React from 'react';


export default class App extends React.Component{
    render(){
        return(
            <div className="todolistapp">
                <div className="addotodo">
                    <AddTodo />
                </div>
                <div className="listtodo">
                    <ListTodo />
                </div>
                <div className="filtertodo">
                    <FilterTodo />
                </div>
            </div>
        );
    }
}

