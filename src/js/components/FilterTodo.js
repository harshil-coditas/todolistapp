import React from "react";
import {VisibilityFilters} from "../constants/action-types"
import {filterTodo} from '../actions/index'
import { connect } from "react-redux";
const mapStateToProps = state =>{
    return {
        filter: state.visibilityFilter
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        filterTodo: filter=> dispatch(filterTodo(filter))
    }
}
class TodoFilter extends React.Component{
    render(){
        return(
            <div className="filterTodos">
                <button onClick={()=>this.props.filterTodo(VisibilityFilters.SHOW_ALL)}>
                    All
                </button>
                <button onClick={()=>this.props.filterTodo(VisibilityFilters.SHOW_ACTIVE)}>
                    Active
                </button>
                <button onClick={()=>this.props.filterTodo(VisibilityFilters.SHOW_COMPLETED)}>
                    Done
                </button> 
            </div>
        );
    }
}

const FilterTodo= connect(mapStateToProps,mapDispatchToProps)(TodoFilter);
export default FilterTodo;

