import React from "react";
import {connect} from "react-redux";
import {toggleTodo} from '../actions/index';
import {ADD_TODO,FILTER_TODO,TOGGLE_TODO, VisibilityFilters} from '../constants/action-types';

const mapStateToProps= state =>{
    return {
        todos:getVisibleTodos(state.todos,state.visibilityFilter)
    }
};

function getVisibleTodos(todos, filter){
    switch(filter){
        case VisibilityFilters.SHOW_ACTIVE: return todos.filter(t => !t.completed)
        case VisibilityFilters.SHOW_COMPLETED: return todos.filter(t => t.completed)
        case VisibilityFilters.SHOW_ALL:
        default: return todos;
    }
}


const mapDispatchToProps = dispatch =>{
    return{
        toggleTodo: id => dispatch(toggleTodo(id))
    }
}


class TodoList extends React.Component{
    constructor(){
        super();
        this.handleClick=this.handleClick.bind(this);
    }

    handleClick(id){
        this.props.toggleTodo(id);

    }
    
    render(){
        return (
            <ul>
                {
                    this.props.todos.map((todo)=>{
                    console.log(todo.completed);
                    return (
                        <li style={{
                            textDecoration:(todo.completed?"line-through":"none")
                        }}
                         key={todo.id} onClick={()=>this.handleClick(todo.id)}>{todo.todo}</li>
                    );
                })}
            </ul>
        );
    }
}

const ListTodo= connect(mapStateToProps,mapDispatchToProps)(TodoList);

export default ListTodo;