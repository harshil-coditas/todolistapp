import {
    VisibilityFilters,
    ADD_TODO,
    FILTER_TODO,
    TOGGLE_TODO
} from '../constants/action-types'

const initialState = {
    todos: [

    ],
    visibilityFilter: VisibilityFilters.SHOW_ALL
}
 const rootReducer = (state = initialState,   action) => {

    switch (action.type) {
        case ADD_TODO:
            {
                return { ...state, todos: [...state.todos, action.payload]
                }
            }
            break;
        case TOGGLE_TODO:
            {
                console.log(action.index)
                return { ...state,todos: state.todos.map((todos) => {
                        if (todos.id === action.index) {
                            todos.completed = !todos.completed
                        }
                        return { ...todos
                        }
                    })
                }
            }
            break;
        case FILTER_TODO:
            {
                return { ...state, visibilityFilter: action.filter
                };
            }
            break;
        default:
            return state;
    }
}


export default rootReducer;